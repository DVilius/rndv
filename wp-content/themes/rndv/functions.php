<?php

global $front_page;
$front_page = 48;

/*
 * Add support to WordPress features to theme.
 **/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );


/*
 * Load theme styles.
 **/
function enqueue_theme_styles() {
    wp_enqueue_style( 'style', get_template_directory_uri() .'/style.css' );
    wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css');
}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_styles' );

/*
 * Upload support for SVG images.
 **/
function theme_add_supported_file_types_to_uploads($file_types) {
    $new_file_types = array();
    $new_file_types['svg'] = 'image/svg+xml';

    $file_types = array_merge($file_types, $new_file_types);
    return $file_types;
}
add_action('upload_mimes', 'theme_add_supported_file_types_to_uploads');

/*
 * Display svg media images as thumbnails.
 **/
function theme_display_svg_media_thumbnails($response, $attachment, $meta) {
    if ($response['type'] === 'image' && $response['subtype'] === 'svg+xml' && class_exists('SimpleXMLElement')) {
        try {
            $path = get_attached_file($attachment->ID);

            if (@file_exists($path)) {

                $svg = new SimpleXMLElement(@file_get_contents($path));
                $src = $response['url'];
                $width = (int) $svg['width'];
                $height = (int) $svg['height'];

                //media gallery
                $response['image'] = compact( 'src', 'width', 'height' );
                $response['thumb'] = compact( 'src', 'width', 'height' );

                //media single
                $response['sizes']['full'] = array(
                    'height'        => $height,
                    'width'         => $width,
                    'url'           => $src,
                    'orientation'   => $height > $width ? 'portrait' : 'landscape',
                );
            }
        } catch(Exception $e) {}
    }

    return $response;
}
add_filter('wp_prepare_attachment_for_js', 'theme_display_svg_media_thumbnails', 10, 3);

/*
 * Adjust svg featured image.
 **/
function adjust_svg_featured_image() {
    $url = $_SERVER['REQUEST_URI'];
    if (strpos($url, '/wp-admin/post.php') !== false) { ?>
        <style> .components-responsive-wrapper__content { position: relative !important; } </style>
        <?php
    }

}
add_action( 'init', 'adjust_svg_featured_image' );

/*
 * Add meta boxes for custom fields.
 **/
function theme_meta_box() {
    global $post;
    global $front_page;
    if ($post->ID == $front_page) {
        add_meta_box('page_lead_text', esc_html__( 'Footer options' ), 'front_page_info', 'page', 'normal', 'default');
    }

    if (get_post_type($post->ID) == "post") {
        add_meta_box('page_lead_text', esc_html__( 'Nuoroda' ), 'post_meta_info', 'post', 'normal', 'default');
    }
}
add_action( 'add_meta_boxes', 'theme_meta_box' );

/*
 * Add custom fields to post pages.
 **/
function post_meta_info( $object, $box ) {
    wp_nonce_field( basename( __FILE__ ), 'post_meta_info' );

    $url = esc_attr(get_post_meta( $object->ID, 'post_page_url', true ));
    ?>
    <p><?php echo _e('Nuoroda', 'rndv') ?></p>
    <label><input type="text"
           class="regular-text"
           name="post_page_url"
           value="<?php echo $url; ?>"></label>
    <?php
}

/*
 * Supported languages.
 **/
global $languages;
$languages = array("lt");
if (function_exists('pll_languages_list')) {
    $languages = pll_languages_list(array('show_names' => 0, 'hide_if_empty'  => 0));
}

/*
 * Add custom fields to front page.
 **/
function front_page_info( $object, $box ) {
    wp_nonce_field( basename( __FILE__ ), 'front_page_info' );

    global $languages;
    foreach ($languages as $lang) :
        $phone = esc_attr( get_post_meta( $object->ID, 'footer_phone_'. $lang, true ));
        $email = esc_attr( get_post_meta( $object->ID, 'footer_email_'. $lang, true ));
        $safe = esc_attr( get_post_meta( $object->ID, 'footer_safe_'. $lang, true ));
        ?>

        <div style="width: 32%; display: inline-block;">
            <h3><?php echo strtoupper($lang) ?></h3>

            <p><?php echo _e('Tekstas', 'rndv') ?></p>
            <label><input type="text"
                   class="regular-text"
                   name="footer_safe_<?php echo $lang ?>"
                   value="<?php echo $safe; ?>"></label>

            <p><?php echo _e('Telefonas', 'rndv') ?></p>
            <label><input type="text"
                   class="regular-text"
                   name="footer_phone_<?php echo $lang ?>"
                   value="<?php echo $phone; ?>"></label>

            <p><?php echo _e('Paštas', 'rndv') ?></p>
            <label><input type="text"
                   class="regular-text"
                   name="footer_email_<?php echo $lang ?>"
                   value="<?php echo $email; ?>"></label>
        </div>
    <?php endforeach;
}

/*
 * Save / update custom fields.
 **/
function theme_meta_box_save($post_id, $post) {

    $map = array(
        'post_meta_info'  => array('post_page_url'),
        'front_page_info' => array()
    );

    global $languages;
    foreach ($languages as $lang) {
        $inputs = array('footer_safe', 'footer_phone', 'footer_email');
        foreach ($inputs as $field) {
            array_push($map['front_page_info'], $field ."_". $lang);
        }
    }

    foreach (array_keys($map) as $key) {
        if (isset( $_POST["$key"] ) && wp_verify_nonce( $_POST["$key"], basename( __FILE__ ) )) {

            $post_type = get_post_type_object( $post->post_type );
            if ( !current_user_can( $post_type->cap->edit_post, $post_id ) ) return $post_id;

            foreach ($map[$key] as $field) {

                $new_meta_value = ( isset( $_POST["$field"] ) ? $_POST["$field"] : '' );
                $meta_key = $field;
                $meta_value = get_post_meta( $post_id, $meta_key, true );

                if ( $new_meta_value && '' == $meta_value ) {
                    add_post_meta( $post_id, $meta_key, $new_meta_value, true );
                } elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
                    update_post_meta( $post_id, $meta_key, $new_meta_value );
                } elseif ( '' == $new_meta_value && $meta_value ) {
                    delete_post_meta( $post_id, $meta_key, $meta_value );
                }
            }
        }
    }

    return $post_id;
}
add_action( 'save_post', 'theme_meta_box_save', 10, 2 );