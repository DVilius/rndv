<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <title><?php bloginfo( 'name' ); ?></title>

    <?php wp_head(); ?>
</head>
<body>
    <div class="gradient"></div>

    <div class="space">
        <div class="language-wrapper">
            <ul class="language">
                <?php if (function_exists('pll_the_languages')) {
                    pll_the_languages(array('show_names' => 0, 'hide_if_empty' => 0, 'dropdown' => 1));
                } ?>
            </ul>
        </div>

        <div class="logo">
            <svg viewBox="0 0 1920 420" fill="none" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <linearGradient id="gradient" x1="0%" y1="0%" x2="100%" y2="0">
                        <stop offset="0%" style="stop-color:#717791;"/>
                        <stop offset="25%" style="stop-color:#91979c;"/>
                        <stop offset="50%" style="stop-color:#d1d7eb;"/>
                        <stop offset="75%" style="stop-color:#616368;"/>
                        <stop offset="100%" style="stop-color:#717791;"/>
                    </linearGradient>
                    <!-- Stitch 2 gradients together for seamless animation  -->
                    <pattern id="pattern" x="0" y="0" width="300%" height="100%" patternUnits="userSpaceOnUse">
                        <rect x="0" y="0" width="151%" height="100%" fill="url(#gradient)">
                            <animate id="op" attributeType="XML"
                                     attributeName="x"
                                     from="0" to="150%"
                                     dur="2s"
                                     repeatCount="2" begin="1s;op.end+60s" />
                        </rect>
                        <rect x="-150%" y="0" width="151%" height="100%" fill="url(#gradient)">
                            <animate id="op2" attributeType="XML"
                                     attributeName="x"
                                     from="-150%" to="0"
                                     dur="2s"
                                     repeatCount="2" begin="1s;op2.end+60s" />
                        </rect>
                    </pattern>
                </defs>
                <path fill="url('#pattern')" clip-rule="evenodd" d="M0 72.4984V0H226.934C290.279 0 337.596 15.1038 370.413 45.3115C397.888 70.2328 412.389 104.216 413.152 144.997C414.679 177.47 405.52 206.167 387.204 231.089C366.598 259.031 336.833 277.911 297.147 288.483L423.837 413.09H316.227L199.459 297.546H42.2417V225.802H237.618C271.962 225.802 297.91 217.495 315.464 200.881C330.728 185.777 338.359 167.653 336.833 145.752C333.78 96.6646 300.963 72.4984 237.618 72.4984H0ZM1920 21.4047V169.812L1788.12 379.106C1771.33 404.783 1756.83 418.376 1744.62 419.887C1730.88 421.397 1714.86 407.804 1697.3 379.106L1464.53 0H1558.4L1744.62 307.363L1920 21.4047ZM568.08 126.872L835.196 392.7C851.987 409.314 867.25 417.621 880.225 417.621C898.541 417.621 907.7 404.028 907.7 377.596V0H835.196V286.973L568.08 21.1454C554.343 6.79673 540.605 0 526.868 0C517.709 0 510.078 3.02077 503.972 9.81749C498.63 16.6142 495.577 24.9213 495.577 36.2492V413.09H568.08V126.872ZM1016.84 0V413.09H1253.42C1318.3 413.09 1370.96 391.189 1409.88 347.388C1445.75 307.363 1463.3 259.031 1463.3 202.391C1463.3 147.262 1445.75 100.441 1411.4 63.4361C1372.48 21.1454 1319.82 0 1252.66 0H1016.84ZM1089.34 341.347V71.7432H1253.42C1294.64 71.7432 1328.22 85.3367 1354.17 112.524C1377.82 136.69 1389.27 166.897 1390.8 202.391C1392.33 239.396 1381.64 271.869 1358.74 297.546C1332.8 326.998 1297.69 341.347 1252.66 341.347H1089.34Z" />
            </svg>
        </div>

        <div class="container">
            <div class="content row">
                <?php
                $query = new WP_Query(array('post_type' => 'post', 'orderby' => 'menu_order'));
                if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="col-xl-12 element">
                            <a id="canvas" target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'post_page_url', true); ?>">
                                <img width="160" height="54"
                                     title="<?php the_title(); ?>"
                                     src="<?php echo get_the_post_thumbnail_url(); ?>"
                                     alt="<?php the_title(); ?>" />
                                <?php the_content(); ?>
                            </a>
                        </div>
                    <?php endwhile;
                endif; ?>
            </div>
        </div>

        <div class="space-connect"></div>
        <div class="globe-wrapper">
            <?php $video = get_template_directory_uri() .'/assets/globe.mp4'; ?>
            <video width="auto" height="auto" id="globe" class="globe" autoplay loop>
                <source src="<?php echo $video ?>" type="video/webm" />
                <source src="<?php echo $video ?>" type="video/mp4" />
                <source src="<?php echo $video ?>" type="video/mov" />
            </video>
        </div>

        <div class="footer-wrapper">
            <div class="footer">
                <div class="container p-0">
                    <?php
                    global $front_page;
                    $lang = ICL_LANGUAGE_CODE;

                    $safe = get_post_meta($front_page, 'footer_safe_'. $lang, true);
                    $phone = get_post_meta($front_page, 'footer_phone_'. $lang, true);
                    $email = get_post_meta($front_page, 'footer_email_'. $lang, true);
                    ?>
                    <a class="phone" href="tel:<?php echo $phone; ?>"><i class="fas fa-phone"></i> <?php echo $phone ?></a>
                    <a class="email" href="mailto:<?php echo $email; ?>"><i class="fas fa-envelope-open"></i> <?php echo $email ?></a>
                    <p class="copyright"><?php _e('© RNDV') ?> <br /> <?php echo $safe; ?> </p>
                </div>
            </div>
        </div>
    </div>

    <footer><?php wp_footer(); ?></footer>
    <script>
        var video = document.getElementById("globe");
        video.oncanplaythrough = function() {
            video.muted = true;
            video.play();
            video.pause();
            video.play();
            video.playbackRate = 0.5;
        }
    </script>

    <script>
    var x, i, j, selElmnt, a, b, c;
    x = document.getElementsByClassName("language");

    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];

        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;

        x[i].appendChild(a);

        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 0; j < selElmnt.length; j++) {

            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            console.log(selElmnt.options[j].selected);
            if (selElmnt.options[j].selected) continue;

            c.addEventListener("click", function(e) {

                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
                window.location.href = 'http://' + window.location.hostname + '/' + h.textContent.toLowerCase();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);

        a.addEventListener("click", function(e) {

            e.stopPropagation();
            closeAllSelect(this);

            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

    function closeAllSelect(elmnt) {

        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }

    document.addEventListener("click", closeAllSelect);
    </script>
</body>
</html>